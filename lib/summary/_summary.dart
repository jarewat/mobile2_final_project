import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SummaryPartial extends StatefulWidget {
  SummaryPartial({Key? key}) : super(key: key);

  @override
  _SummaryPartialState createState() => _SummaryPartialState();
}

class _SummaryPartialState extends State<SummaryPartial> {
  double balance = 0;
  double income = 0;
  double spend = 0;

  CollectionReference transactions = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('transactions');

  @override
  void initState() {
    initData();
    super.initState();
  }

  Future<void> initData() {
    return transactions.get().then((QuerySnapshot snapshot) {
      snapshot.docs.forEach((doc) {
        setState(() {
          if (doc['type'] == 'income') {
            income = doc['amount'] + income;
          }
          if (doc['type'] == 'spend') {
            spend = doc['amount'] + spend;
          }
          balance = income - spend;
        });
      });
    });
  }

  List<PieSeries<ChartSampleData, String>> _getDataPieSeries(
      double income, double spend) {
    final List<ChartSampleData> pieData = <ChartSampleData>[
      ChartSampleData('รายรับ', income, 'รายรับ \n $income'),
      ChartSampleData('รายจ่าย', spend, 'รายจ่าย \n $spend'),
    ];
    return <PieSeries<ChartSampleData, String>>[
      PieSeries<ChartSampleData, String>(
          explode: true,
          explodeIndex: 0,
          explodeOffset: '5%',
          dataSource: pieData,
          xValueMapper: (ChartSampleData data, _) => data.x,
          yValueMapper: (ChartSampleData data, _) => data.y,
          dataLabelMapper: (ChartSampleData data, _) => data.text,
          startAngle: 90,
          endAngle: 90,
          dataLabelSettings: const DataLabelSettings(isVisible: true)),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xFF25978a),
                      Color(0xFFeca184),
                    ],
                  )),
              margin: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(20, 24, 20, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  'Balance',
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontWeight: FontWeight.w900,
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(20, 8, 20, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  '\฿' + balance.toStringAsFixed(2),
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontWeight: FontWeight.w900,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        width: 75.0,
                        height: 75.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: new NetworkImage(FirebaseAuth
                                .instance.currentUser!.photoURL
                                .toString()),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(20, 12, 20, 16),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          FirebaseAuth.instance.currentUser!.displayName
                              .toString(),
                          style: TextStyle(
                            fontFamily: 'Lexend Deca',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  color: Color(0xFFeca184),
                ),
                margin: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.44,
                          height: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xFF292c2e),
                          ),
                          child: Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(12, 12, 12, 12),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'รายรับ',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontWeight: FontWeight.w900,
                                    fontSize: 16,
                                    color: Color(0xFF8c97a2),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      0, 8, 0, 12),
                                  child: Text(
                                    '+\฿$income',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: 'Lexend Deca',
                                      fontWeight: FontWeight.w900,
                                      fontSize: 20,
                                      color: Color(0xFF4ed3c1),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.44,
                          height: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Color(0xFF292c2e),
                          ),
                          child: Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(12, 12, 12, 12),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'รายจ่าย',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontWeight: FontWeight.w900,
                                    fontSize: 16,
                                    color: Color(0xFF8c97a2),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      0, 8, 0, 12),
                                  child: Text(
                                    '-\฿$spend',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: 'Lexend Deca',
                                      fontWeight: FontWeight.w900,
                                      fontSize: 20,
                                      color: Color(0xFFeb6769),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          height: 250,
                          width: 250,
                          child: income == 0 || spend == 0
                              ? Container()
                              : SfCircularChart(
                                  series: _getDataPieSeries(income, spend),
                                ),
                        ),
                      ],
                    ),
                  ],
                ))
          ],
        )
      ],
    );
  }
}

class ChartSampleData {
  ChartSampleData(this.x, this.y, this.text);
  late final String text;
  late final String x;
  late final double y;
}
