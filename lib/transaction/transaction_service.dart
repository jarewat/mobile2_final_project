import 'package:cloud_firestore/cloud_firestore.dart';
import 'transaction.dart';

CollectionReference transactions =
    FirebaseFirestore.instance.collection('transactions');

Future<void> addNewTransaction(UserTransaction transaction) {
  return transactions
      .add({
        'amount': transaction.amount,
        'category': transaction.category,
        'date': transaction.date,
        'note': transaction.note,
        'uid': transaction.uid,
      })
      .then((value) => print("transaction Added"))
      .catchError((error) => print("Failed to add transaction: $error"));
}

Future<void> saveTransaction(UserTransaction transaction) {
  return transactions
      .doc(transaction.tranId)
      .update({
        'amount': transaction.amount,
        'category': transaction.category,
        'date': transaction.date,
        'note': transaction.note,
        'uid': transaction.uid,
      })
      .then((value) => print("transaction Updated"))
      .catchError((error) => print("Failed to update transaction: $error"));
}

Future<void> delTransaction(UserTransaction transaction) {
  return transactions
      .doc(transaction.tranId)
      .delete()
      .then((value) => print("transaction Deleted"))
      .catchError((error) => print("Failed to delete transaction: $error"));
}

// Future<List<UserTransaction>?> getListTransaction(String uid) async {
//   List<UserTransaction>? list;
//   transactions.get().then((QuerySnapshot querySnapshot) {
//     querySnapshot.docs.forEach((doc) {
//       Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
//       print(UserTransaction.toDataModel(data));
//       list!.add(UserTransaction.toDataModel(data));
//     });
//   });
//   return list;
// }
