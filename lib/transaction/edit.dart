// ignore_for_file: must_be_immutable

import 'package:cloud_firestore/cloud_firestore.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:intl/intl.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flash/flash.dart';

class EditTransaction extends StatefulWidget {
  String? transactionID;
  String? type;
  EditTransaction({Key? key, required this.transactionID}) : super(key: key);

  @override
  _EditTransactionState createState() => _EditTransactionState(transactionID);
}

class _EditTransactionState extends State<EditTransaction> {
  _EditTransactionState(this.transactionID);
  final _formKey = GlobalKey<FormState>();

  String? transactionID;
  String? note;
  int? amount;
  String? category;
  String? type;
  Timestamp? date;
  String? uid;

  TextEditingController _noteController = new TextEditingController();
  TextEditingController _amountController = new TextEditingController();
  TextEditingController _dateController = new TextEditingController();

  List<dynamic> categories = [];
  // ignore: non_constant_identifier_names
  List<dynamic> categories_income = [];
  // ignore: non_constant_identifier_names
  List<dynamic> categories_spend = [];

  // String? typeId;
  String? incomeId;
  String? spendId;

  bool isDeleted = false;
  bool isAdded = false;
  bool isCFAdded = false;

  bool isHasTypeIncome = false;
  bool isHasTypeSpend = false;

  CollectionReference transactions = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('transactions');
  DocumentReference users = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid);

  @override
  initState() {
    getcategories();
    super.initState();
    if (this.transactionID != null)
      transactions.doc(this.transactionID).get().then((document) {
        if (document.exists) {
          Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
          setState(() {
            amount = data['amount'];
            category = data['category'];
            type = data['type'];
            note = data['note'];
            date = data['date'];

            _noteController.text = note!;
            _amountController.text = amount.toString();
            DateFormat formatter = DateFormat('yyyy-MM-dd');
            _dateController.text = formatter.format(
                DateTime.fromMillisecondsSinceEpoch(
                    (data['date']!.millisecondsSinceEpoch)));
          });
        }
      });
  }

  Future<void> getCategoriesIncome() async {
    print('method getCategoriesIncome()');
    await getTypeId('income');
    return users
        .collection('type')
        .doc(incomeId)
        .collection('category')
        .get()
        .then((QuerySnapshot snapshot) {
      List<dynamic> list = [];
      snapshot.docs.forEach((doc) {
        Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
        list.add({
          "display": data['name'].toString(),
          "value": data['name'].toString(),
        });
      });
      setState(() {
        categories_income = list;
      });
    });
  }

  Future<void> getCategoriesSpend() async {
    print('method getCategoriesSpend()');
    await getTypeId('spend');
    return users
        .collection('type')
        .doc(spendId)
        .collection('category')
        .get()
        .then((QuerySnapshot snapshot) {
      List<dynamic> list = [];
      snapshot.docs.forEach((doc) {
        Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
        list.add({
          "display": data['name'].toString(),
          "value": data['name'].toString(),
        });
      });
      setState(() {
        categories_spend = list;
      });
    });
  }

  getcategories() async {
    await getCategoriesIncome();
    print('categories_income : $categories_income');
    await getCategoriesSpend();
    print('categories_spend : $categories_spend');
    if (categories_income.length == 0 && categories_spend.length == 0) {
      await getDefaultCategory(3);
    } else {
      if (categories_income.length == 0) {
        await getDefaultCategory(1);
      }
      if (categories_spend.length == 0) {
        await getDefaultCategory(2);
      }
    }

    if (type == 'income') {
      categories = categories_income;
    }
    if (type == 'spend') {
      categories = categories_spend;
    }
  }

  getDefaultCategory(int type) {
    FirebaseFirestore.instance
        .collection('default_category')
        .get()
        .then((QuerySnapshot snapshot) async {
      print('method : getDefaultCategory($type)');
      List<dynamic> listIncome = [];
      List<dynamic> listSpend = [];
      await addTypes(type);
      snapshot.docs.forEach((doc) {
        Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
        if (data['type'] == 'income' && (type == 1 || type == 3)) {
          users
              .collection('type')
              .doc(incomeId)
              .collection('category')
              .add({'name': data['name']});
          listIncome.add({
            "display": data['name'].toString(),
            "value": data['name'].toString(),
          });
        }
        if (data['type'] == 'spend' && (type == 2 || type == 3)) {
          users
              .collection('type')
              .doc(spendId)
              .collection('category')
              .add({'name': data['name']});
          listSpend.add({
            "display": data['name'].toString(),
            "value": data['name'].toString(),
          });
        }
      });
      setState(() {
        if (type == 1 || type == 3) categories_income = listIncome;
        if (type == 2 || type == 3) categories_spend = listSpend;
      });
    });
  }

  Future<void> getTypeId(String typeID) async {
    print('Method : getTypeId($typeID)');
    return await users
        .collection('type')
        .where('name', isEqualTo: typeID)
        .get()
        .then((QuerySnapshot snapshot) async {
      snapshot.docs.forEach((doc) {
        Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
        setState(() {
          if (data['name'] == 'income') {
            incomeId = doc.id;
          }
          if (data['name'] == 'spend') {
            spendId = doc.id;
          }
        });
      });
      if (incomeId == null && spendId == null)
        await addTypes(3);
      else if (incomeId == null)
        await addTypes(1);
      else if (spendId == null) await addTypes(2);
      print('incomeId : $incomeId');
      print('spendId : $spendId');
    });
  }

  Future<void> checkType() {
    print('Method : checkType');
    return users.collection('type').get().then((QuerySnapshot snapshot) {
      snapshot.docs.forEach((doc) {
        Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
        if (data['name'] == 'income') {
          isHasTypeIncome = true;
          print('income is true');
        }
        if (data['name'] == 'spend') {
          isHasTypeSpend = true;
          print('spend is true');
        }
      });
    });
  }

  addTypes(int type) async {
    await checkType();
    if (type == 3 && !isHasTypeSpend && !isHasTypeIncome) {
      print('add 3');
      await users.collection('type').add({'name': 'income'});
      await users.collection('type').add({'name': 'spend'});
    } else if (type == 1 && !isHasTypeIncome) {
      print('add 1');
      await users.collection('type').add({'name': 'income'});
    } else if (type == 2 && !isHasTypeSpend) {
      print('add 2');
      await users.collection('type').add({'name': 'spend'});
    }
  }

  Future<void> updateTransaction() {
    return transactions
        .doc(this.transactionID)
        .update({
          'type': this.type,
          'category': this.category,
          'amount': this.amount,
          'note': this.note,
          'date': this.date
        })
        .then((value) => print("Transaction Updated"))
        .catchError((error) => throw error);
  }

  Future<void> addTransaction() {
    return transactions.add({
      'amount': this.amount,
      'category': this.category,
      'type': this.type,
      'note': this.note,
      'date': this.date
    }).then((value) {
      isAdded = true;
    }).catchError((error) => throw error);
  }

  Future<void> deleteTransaction(tranId) {
    return transactions
        .doc(tranId)
        .delete()
        .then((value) => print("Transaction Deleted"))
        .catchError((error) => throw error);
  }

  void _showBasicsFlash({
    TextStyle? textStyle,
    required String text,
    Duration? duration,
    flashStyle = FlashBehavior.floating,
  }) {
    showFlash(
      context: context,
      duration: duration,
      builder: (context, controller) {
        return Flash(
          controller: controller,
          behavior: flashStyle,
          position: FlashPosition.bottom,
          boxShadows: kElevationToShadow[4],
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: FlashBar(
            content: Text(
              text,
              style: textStyle,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NewGradientAppBar(
        title: Text((transactionID != null ? 'Edit' : 'Add') + ' transaction'),
        gradient: LinearGradient(
          colors: [
            Color(0xFFeea184),
            Color(0xFF24978a),
          ],
        ),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Container(
              padding: EdgeInsets.all(16),
              child: DropDownFormField(
                validator: (value) {
                  if (type == null) {
                    return 'กรุณากรอกประเภท';
                  }
                  return null;
                },
                titleText: 'ประเภท',
                value: type,
                onSaved: (value) {
                  setState(() {
                    type = value;
                  });
                },
                onChanged: (value) {
                  setState(() {
                    category = null;
                    type = value;
                    if (value == 'income') {
                      categories = categories_income;
                    } else {
                      categories = categories_spend;
                    }
                  });
                },
                dataSource: [
                  {
                    "display": "รายรับ",
                    "value": "income",
                  },
                  {
                    "display": "รายจ่าย",
                    "value": "spend",
                  }
                ],
                textField: 'display',
                valueField: 'value',
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: DropDownFormField(
                validator: (value) {
                  if (category == null) {
                    return 'กรุณากรอกหมวดหมู่';
                  }
                  return null;
                },
                titleText: 'หมวดหมู่',
                value: category,
                onSaved: (value) {
                  setState(() {
                    category = value;
                  });
                },
                onChanged: (value) {
                  setState(() {
                    category = value;
                  });
                },
                dataSource: categories,
                textField: 'display',
                valueField: 'value',
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: TextFormField(
                controller: _amountController,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (value) {
                  setState(() {
                    amount = int.parse(value);
                  });
                },
                onSaved: (value) {
                  setState(() {
                    amount = int.parse(value!);
                  });
                },
                decoration: InputDecoration(
                  labelText: 'จำนวน',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'กรุณากรอกจำนวน';
                  }
                  return null;
                },
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: TextFormField(
                controller: _noteController,
                onChanged: (value) {
                  setState(() {
                    note = value;
                  });
                },
                onSaved: (value) {
                  setState(() {
                    note = value;
                  });
                },
                decoration: InputDecoration(
                  labelText: 'รายละเอียด',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'กรุณากรอกรายละเอียด';
                  }
                  return null;
                },
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: TextFormField(
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2015, 3, 5),
                      maxTime: DateTime(2030, 6, 7), onChanged: (value) {
                    setState(() {
                      DateFormat formatter = DateFormat('yyyy-MM-dd');
                      _dateController.text = formatter.format(value);
                      date = Timestamp.fromDate(value);
                    });
                  }, onConfirm: (value) {
                    setState(() {
                      DateFormat formatter = DateFormat('yyyy-MM-dd');
                      _dateController.text = formatter.format(value);
                      date = Timestamp.fromDate(value);
                    });
                  },
                      currentTime: transactionID != null
                          ? DateTime.fromMillisecondsSinceEpoch(
                              (date!.millisecondsSinceEpoch))
                          : DateTime.now(),
                      locale: LocaleType.th);
                },
                readOnly: true,
                controller: _dateController,
                decoration: InputDecoration(
                  labelText: 'วันที่',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'กรุณากรอกวันที่';
                  }
                  return null;
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                transactionID == null
                    ? new Container()
                    : ElevatedButton(
                        onPressed: () {
                          Alert(
                            context: context,
                            type: AlertType.warning,
                            title: "แจ้งเตือน",
                            desc: "ต้องการลบข้อมูลนี้หรือไม่.",
                            buttons: [
                              DialogButton(
                                child: Text(
                                  "ใช่",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onPressed: () async {
                                  await deleteTransaction(transactionID);
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                  _showBasicsFlash(
                                      text: 'ลบข้อมูลเรียบร้อย',
                                      textStyle:
                                          TextStyle(color: Color(0xFFeb6769)),
                                      duration: Duration(seconds: 2),
                                      flashStyle: FlashBehavior.fixed);
                                  Navigator.pop(context);
                                  // Navigator.pop(context);
                                },
                                color: Color.fromRGBO(0, 179, 134, 1.0),
                              ),
                              DialogButton(
                                child: Text(
                                  "ยกเลิก",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                onPressed: () =>
                                    Navigator.of(context, rootNavigator: true)
                                        .pop(),
                                gradient: LinearGradient(colors: [
                                  Color.fromRGBO(116, 116, 191, 1.0),
                                  Color.fromRGBO(52, 138, 199, 1.0)
                                ]),
                              )
                            ],
                          ).show();
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              Color.fromRGBO(175, 39, 86, 1.0)),
                        ),
                        child: Text('ลบ'),
                      ),
                transactionID == null
                    ? new Container()
                    : SizedBox(
                        width: 10,
                      ),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                        Color.fromRGBO(0, 179, 134, 1.0)),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (transactionID == null) {
                        await addTransaction();
                        _showBasicsFlash(
                            text: 'บันทึกข้อมูลเรียบร้อย',
                            textStyle: TextStyle(color: Color(0xFF4ed3c1)),
                            duration: Duration(seconds: 2),
                            flashStyle: FlashBehavior.fixed);
                        Navigator.pop(context);
                      } else {
                        await updateTransaction();
                        _showBasicsFlash(
                            text: 'บันทึกข้อมูลเรียบร้อย',
                            textStyle: TextStyle(color: Color(0xFF4ed3c1)),
                            duration: Duration(seconds: 2),
                            flashStyle: FlashBehavior.fixed);
                        Navigator.pop(context);
                      }
                    }
                  },
                  child: Text('บันทึก'),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
