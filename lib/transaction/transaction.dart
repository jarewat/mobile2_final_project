import 'package:cloud_firestore/cloud_firestore.dart';

class UserTransaction {
  int amount;
  String category;
  Timestamp date;
  String note;
  String uid;
  String? tranId;

  UserTransaction(
      {required this.amount,
      required this.category,
      required this.date,
      required this.note,
      required this.uid});

  Map<String, dynamic> toMap() {
    return {
      'amount': amount,
      'category': category,
      'date': date,
      'note': note,
      'uid': uid,
    };
  }

  static List<UserTransaction> toList(List<Map<String, dynamic>> maps) {
    return List.generate(
      maps.length,
      (i) {
        return UserTransaction(
            amount: maps[i]['amount'],
            category: maps[i]['category'],
            date: maps[i]['date'],
            note: maps[i]['note'],
            uid: maps[i]['uid']);
      },
    );
  }

  static UserTransaction toDataModel(Map<String, dynamic> data) {
    return UserTransaction(
        amount: data['amount'],
        category: data['category'],
        date: data['date'],
        note: data['note'],
        uid: data['uid']);
  }

  @override
  String toString() {
    return 'UserTransaction {amount: $amount, category :$category, date: $date, note: $note, uid: $uid}';
  }
}
