import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:income_and_expenses/transaction/edit.dart';
import 'package:intl/intl.dart';

class TransactionPartial extends StatefulWidget {
  TransactionPartial({Key? key}) : super(key: key);

  @override
  _TransactionPartialState createState() => _TransactionPartialState();
}

class _TransactionPartialState extends State<TransactionPartial> {
  final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('transactions')
      .orderBy('date', descending: true)
      .snapshots();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text(
            'Something went wrong',
            style: TextStyle(
              fontFamily: 'Lexend Deca',
            ),
          );
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return SpinKitDualRing(
            color: Color(0xFF24978a),
            size: 50.0,
          );
        }
        return ListView(
          children: snapshot.data!.docs.map(
            (DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditTransaction(
                        transactionID: document.id,
                      ),
                    ),
                  );
                },
                child: Container(
                  margin: const EdgeInsets.fromLTRB(6, 10, 6, 0),
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color(0xFFe4e7e2),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        child: Card(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          color: data['type'] == 'income'
                              ? Color(0xFF24978a)
                              : Color(0xFFeea184),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                            child: Icon(
                              Icons.monetization_on_rounded,
                              size: 18,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  data['note'] ?? "",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Lexend Deca',
                                    fontSize: 20,
                                  ),
                                ),
                                Text(
                                  data['amount'].toString(),
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontSize: 18,
                                    color: data['type'] == 'income'
                                        ? Color(0xFF24978a)
                                        : Color(0xFFeb6769),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'หมวดหมู่ ' + (data['category'] ?? ""),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Lexend Deca',
                                  ),
                                ),
                                data['date'] == null
                                    ? Text('')
                                    : Text(
                                        'วันที่ ' +
                                            DateFormat('yyyy-MM-dd').format(DateTime
                                                .fromMicrosecondsSinceEpoch(data[
                                                        'date']
                                                    .microsecondsSinceEpoch)),
                                        textAlign: TextAlign.end,
                                        style: TextStyle(
                                          fontFamily: 'Lexend Deca',
                                          color: data['type'] == 'income'
                                              ? Color(0xFF24978a)
                                              : Color(0xFFeb6769),
                                        ),
                                      ),
                              ],
                            ),
                          ],
                        ),
                      ))
                    ],
                  ),
                ),
              );
            },
          ).toList(),
        );
      },
    );
  }
}
