import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SettingPartial extends StatefulWidget {
  SettingPartial({Key? key}) : super(key: key);

  @override
  _SettingPartialState createState() => _SettingPartialState();
}

class _SettingPartialState extends State<SettingPartial> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () async {
                FirebaseAuth.instance.signOut();
              },
              child: new Row(
                children: [
                  Ink(
                    padding: EdgeInsets.only(bottom: 5),
                    color: Color.fromRGBO(175, 39, 86, 1.0),
                    width: 240,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            'Sign out',
                            style: TextStyle(
                              fontFamily: 'Lexend Deca',
                              fontWeight: FontWeight.w900,
                              fontSize: 16,
                              color: Color(0xFFf2f9f8),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
