import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:income_and_expenses/screen/regisPage.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  String? email;
  String? password;

  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<void>? saveUser(User user) {
    print(user);
    // users
    //     .where('uid', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
    //     .get()
    //     .then((QuerySnapshot querySnapshot) {
    //   querySnapshot.docs.forEach((element) {
    //     element.id;
    //     users
    //         .doc(element.id)
    //         .collection('transactions')
    //         .get()
    //         .then((QuerySnapshot querySnapshot) {
    //       querySnapshot.docs.forEach((doc) {
    //         // Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
    //         print(doc.data());
    //       });
    //     });
    //   });
    // });

    // users
    //     .doc(FirebaseAuth.instance.currentUser!.uid)
    //     .collection('transactions')
    //     .add({
    //   'amount': 50,
    //   'category': "อาหาร",
    //   'date': DateTime.now(),
    //   'note': "ข้าวขาหมู",
    //   'type': "รายรับ"
    // }).then((value) => print(value));

    // users
    //     .doc(querySnapshot1.docs)
    //     .collection('transactions')
    //     .get()
    //     .then((QuerySnapshot querySnapshot) {
    //   querySnapshot.docs.forEach((doc) {
    //     Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
    //     print(doc.data());
    //   });
    // });

    // users
    //     .doc(FirebaseAuth.instance.currentUser!.uid)
    //     .collection('transactions')
    //     .get()
    //     .then((QuerySnapshot querySnapshot) {
    //   querySnapshot.docs.forEach((doc) {
    //     Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
    //     print(doc.data());
    //   });
    // });
    // users
    //     .where('email', isEqualTo: 'latealone999@gmail.com')
    //     .get()
    //     .then((QuerySnapshot querySnapshot) {
    //   querySnapshot.docs.forEach((doc) {
    //     Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;
    //     print(data['email']);
    //   });
    // });
  }

  Future<UserCredential> signInWithGoogle() async {
    GoogleAuthProvider googleProvider = GoogleAuthProvider();
    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);
  }

  void _showBasicsFlash({
    TextStyle? textStyle,
    required String text,
    Duration? duration,
    flashStyle = FlashBehavior.floating,
  }) {
    showFlash(
      context: context,
      duration: duration,
      builder: (context, controller) {
        return Flash(
          controller: controller,
          behavior: flashStyle,
          position: FlashPosition.bottom,
          boxShadows: kElevationToShadow[4],
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: FlashBar(
            content: Text(
              text,
              style: textStyle,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: ListView(
          children: [
            Image.asset(
              'assets/login_icon.png',
              width: 200,
              height: 90,
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      controller: _emailController,
                      onChanged: (value) {
                        setState(() {
                          email = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณากรอกอีเมล';
                        }
                        return null;
                      },
                      style: TextStyle(
                        fontFamily: 'Lexend Deca',
                        color: Color(0x98FFFFFF),
                      ),
                      obscureText: false,
                      decoration: InputDecoration(
                        labelText: 'อีเมล',
                        labelStyle: TextStyle(
                          fontFamily: 'Lexend Deca',
                          color: Color(0x98FFFFFF),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        filled: true,
                        contentPadding:
                            EdgeInsetsDirectional.fromSTEB(20, 24, 20, 24),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      style: TextStyle(
                        fontFamily: 'Lexend Deca',
                        color: Color(0x98FFFFFF),
                      ),
                      obscureText: true,
                      controller: _passwordController,
                      onChanged: (value) {
                        setState(() {
                          password = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณากรอกรหัสผ่าน';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: 'รหัสผ่าน',
                        labelStyle: TextStyle(
                          fontFamily: 'Lexend Deca',
                          color: Color(0x98FFFFFF),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        filled: true,
                        contentPadding:
                            EdgeInsetsDirectional.fromSTEB(20, 24, 20, 24),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () async {
                          if (_formKey.currentState!.validate()) {
                            try {
                              await FirebaseAuth.instance
                                  .signInWithEmailAndPassword(
                                      email: email!,
                                      password: password!);
                            } on FirebaseAuthException catch (e) {
                              print('Failed with error code: ${e.code}');
                              if (e.code == 'user-not-found') {
                                _showBasicsFlash(
                                    text: 'ไม่พบผู้ใช้งานนี้',
                                    textStyle:
                                        TextStyle(color: Color(0xFF31a3b8)),
                                    duration: Duration(seconds: 2),
                                    flashStyle: FlashBehavior.fixed);
                              } else if (e.code == 'wrong-password') {
                                _showBasicsFlash(
                                    text: 'รหัสผ่านไม่ถูกต้อง',
                                    textStyle:
                                        TextStyle(color: Color(0xFFd62f43)),
                                    duration: Duration(seconds: 2),
                                    flashStyle: FlashBehavior.fixed);
                              }
                            }
                          }
                        },
                        child: Ink(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            color: Color(0xFF24978a),
                            borderRadius: BorderRadius.circular(0),
                          ),
                          width: 240,
                          child: Padding(
                            padding: EdgeInsets.all(6),
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Text(
                                  'เข้าสู่ระบบ',
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontWeight: FontWeight.w900,
                                    fontSize: 16,
                                    color: Color(0x98FFFFFF),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () async {
                      await signInWithGoogle();
                    },
                    child: new Row(
                      children: [
                        Ink(
                          color: Colors.white,
                          width: 240,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(14, 6, 14, 6),
                            child: Wrap(
                              alignment: WrapAlignment.start,
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/google_icon.png',
                                  width: 25,
                                  height: 25,
                                ),
                                SizedBox(width: 30),
                                Text('เข้าสู่ระบบด้วย Google'),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RegisPage(),
                      ),
                    );
                  },
                  child: Ink(
                    padding: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                      color: Color(0xFF986234),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    width: 180,
                    child: Padding(
                      padding: EdgeInsets.all(7),
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Text(
                            'สร้างบัญชีใหม่',
                            style: TextStyle(
                              fontFamily: 'Lexend Deca',
                              fontWeight: FontWeight.w900,
                              fontSize: 14,
                              color: Color(0x98FFFFFF),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        backgroundColor: Colors.transparent,
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                'Copyright © MoneyLover Company',
                style: TextStyle(
                  color: Color(0xFF05948b),
                ),
              ),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFFeea184),
            Color(0xFF24978a),
          ],
        ),
      ),
    );
  }
}
