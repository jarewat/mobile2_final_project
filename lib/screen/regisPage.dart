import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class RegisPage extends StatefulWidget {
  RegisPage({Key? key}) : super(key: key);

  @override
  _RegisPageState createState() => _RegisPageState();
}

class _RegisPageState extends State<RegisPage> {
  final _formKey = GlobalKey<FormState>();
  String? email;
  String? password;
  String? CFPassword;
  String? fileName;
  String? imageURL;
  String? displayName;
  PlatformFile? temp;
  String? tempImageURL;

  TextEditingController _displayNameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _CFPasswordController = new TextEditingController();

  Future<void> uploadFile(PlatformFile file, String fileN) async {
    print('methos : uploadFile(fileName : $fileN)');
    if (fileName == null) fileName = fileN;
    if (temp == null) temp = file;
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('images/$fileN')
          .putData(file.bytes!);
      await downloadURLExample();
    } on FirebaseException catch (e) {
      print(e);
    }
  }

  Future<void> downloadURLExample() async {
    print('methos : downloadURLExample() \n var : filename $fileName');
    String url = '';
    try {
      url = await firebase_storage.FirebaseStorage.instance
          .ref('images/$fileName')
          .getDownloadURL();
    } on FirebaseException catch (e) {
      print('Error on downloadURLExample() : $e');
    }
    if (this.mounted) {
      setState(() {
        imageURL = url;
      });
    }
    print('imageURL : $imageURL');
  }

  Future<void> downloadTempURL(String uid) async {
    print(
        'methos : downloadTempURL() \n var : uid $uid \n file name : $uid.${fileName!.split(".").last}');
    try {
      String url = await firebase_storage.FirebaseStorage.instance
          .ref('images/$uid.${fileName!.split(".").last}')
          .getDownloadURL();
      tempImageURL = url;
      print('tempImageURL : $tempImageURL');
    } on FirebaseException catch (e) {
      print('Error on downloadTempURL() : $e');
    }
  }

  void _showBasicsFlash({
    TextStyle? textStyle,
    required String text,
    Duration? duration,
    flashStyle = FlashBehavior.floating,
  }) {
    showFlash(
      context: context,
      duration: duration,
      builder: (context, controller) {
        return Flash(
          controller: controller,
          behavior: flashStyle,
          position: FlashPosition.bottom,
          boxShadows: kElevationToShadow[4],
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: FlashBar(
            content: Text(
              text,
              style: textStyle,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('สร้างบัญชี'),
          backgroundColor: Color(0xFFeea184),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      style: TextStyle(
                        fontFamily: 'Lexend Deca',
                        color: Color(0x98FFFFFF),
                      ),
                      controller: _displayNameController,
                      onChanged: (value) {
                        setState(() {
                          displayName = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณากรอกชื่อ';
                        }
                        return null;
                      },
                      obscureText: false,
                      decoration: InputDecoration(
                        labelText: 'ชื่อ',
                        labelStyle: TextStyle(
                          fontFamily: 'Lexend Deca',
                          color: Color(0x98FFFFFF),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        filled: true,
                        contentPadding:
                            EdgeInsetsDirectional.fromSTEB(20, 24, 20, 24),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      style: TextStyle(
                        fontFamily: 'Lexend Deca',
                        color: Color(0x98FFFFFF),
                      ),
                      controller: _emailController,
                      onChanged: (value) {
                        setState(() {
                          email = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณากรอกอีเมล';
                        }
                        return null;
                      },
                      obscureText: false,
                      decoration: InputDecoration(
                        labelText: 'อีเมล',
                        labelStyle: TextStyle(
                          fontFamily: 'Lexend Deca',
                          color: Color(0x98FFFFFF),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        filled: true,
                        contentPadding:
                            EdgeInsetsDirectional.fromSTEB(20, 24, 20, 24),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      style: TextStyle(
                        fontFamily: 'Lexend Deca',
                        color: Color(0x98FFFFFF),
                      ),
                      obscureText: true,
                      controller: _passwordController,
                      onChanged: (value) {
                        setState(() {
                          password = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณากรอกรหัสผ่าน';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: 'รหัสผ่าน',
                        labelStyle: TextStyle(
                          fontFamily: 'Lexend Deca',
                          color: Color(0x98FFFFFF),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        filled: true,
                        contentPadding:
                            EdgeInsetsDirectional.fromSTEB(20, 24, 20, 24),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      style: TextStyle(
                        fontFamily: 'Lexend Deca',
                        color: Color(0x98FFFFFF),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      obscureText: true,
                      controller: _CFPasswordController,
                      onChanged: (value) {
                        setState(() {
                          CFPassword = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'ยืนยันรหัสผ่าน';
                        }
                        if (value != password) {
                          return 'กรุณากรอกรหัสผ่านให้ตรงกัน';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: 'ยืนยัน',
                        labelStyle: TextStyle(
                          fontFamily: 'Lexend Deca',
                          color: Color(0x98FFFFFF),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0x00000000),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        filled: true,
                        contentPadding:
                            EdgeInsetsDirectional.fromSTEB(20, 24, 20, 24),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Column(
                      children: [
                        Text(
                          imageURL != null ? '' : 'อัพโหลดรูป ',
                          style: TextStyle(
                            fontFamily: 'Lexend Deca',
                            color: Color(0x98FFFFFF),
                          ),
                        ),
                        InkWell(
                          onTap: () async {
                            FilePickerResult? result =
                                await FilePicker.platform.pickFiles();
                            if (result != null) {
                              await uploadFile(result.files.single,
                                  result.files.single.name);
                            } else {
                              // User canceled the picker
                            }
                          },
                          child: Container(
                            child: imageURL != null
                                ? Image.network(
                                    imageURL!,
                                    height: 200,
                                    width: 200,
                                  )
                                : Icon(
                                    Icons.image_outlined,
                                    size: 30,
                                  ),
                          ),
                        ),
                        Text(
                          imageURL != null ? 'รูปโปรไฟล์' : '',
                          style: TextStyle(
                            fontFamily: 'Lexend Deca',
                            color: Color(0x98FFFFFF),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () async {
                          if (_formKey.currentState!.validate()) {
                            try {
                              UserCredential userCredential = await FirebaseAuth
                                  .instance
                                  .createUserWithEmailAndPassword(
                                      email: email!, password: CFPassword!);
                              String uid =
                                  FirebaseAuth.instance.currentUser!.uid;
                              await uploadFile(
                                  temp!, uid + '.' + fileName!.split(".").last);
                              firebase_storage.FirebaseStorage.instance
                                  .refFromURL(imageURL!)
                                  .delete();
                              await downloadTempURL(uid);
                              print('uid : $uid');
                              await FirebaseAuth.instance.currentUser!
                                  .updatePhotoURL(tempImageURL);
                              await FirebaseAuth.instance.currentUser!
                                  .updateDisplayName(displayName);
                            } on FirebaseAuthException catch (e) {
                              if (e.code == 'weak-password') {
                                _showBasicsFlash(
                                    text: 'The password provided is too weak.',
                                    textStyle:
                                        TextStyle(color: Color(0xFFd62f43)),
                                    duration: Duration(seconds: 3),
                                    flashStyle: FlashBehavior.fixed);
                              } else if (e.code == 'email-already-in-use') {
                                _showBasicsFlash(
                                    text:
                                        'The account already exists for that email.',
                                    textStyle:
                                        TextStyle(color: Color(0xFFd62f43)),
                                    duration: Duration(seconds: 3),
                                    flashStyle: FlashBehavior.fixed);
                              }
                            }
                          }
                        },
                        child: Ink(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            color: Color(0xFF24978a),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          width: 240,
                          child: Padding(
                            padding: EdgeInsets.all(6),
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Text(
                                  'สร้างบัญชีใหม่',
                                  style: TextStyle(
                                    fontFamily: 'Lexend Deca',
                                    fontWeight: FontWeight.w900,
                                    fontSize: 14,
                                    color: Color(0x98FFFFFF),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        backgroundColor: Colors.transparent,
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                'Copyright © MoneyLover Company',
                style: TextStyle(
                  color: Color(0xFF05948b),
                ),
              ),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFFeea184),
            Color(0xFF24978a),
          ],
        ),
      ),
    );
  }
}
