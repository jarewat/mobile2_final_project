import 'package:flutter/material.dart';
import 'package:income_and_expenses/setting/_setting.dart';
import 'package:income_and_expenses/summary/_summary.dart';
import 'package:income_and_expenses/transaction/_transaction.dart';
import 'package:income_and_expenses/transaction/edit.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  // ignore: unused_field
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Container> _widgetOptions = <Container>[
    Container(
      child: SummaryPartial(),
    ),
    Container(
      child: TransactionPartial(),
    ),
    Container(
      child: SettingPartial(),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<String> texts = ['Summary', 'Transactions', 'Setting'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NewGradientAppBar(
        title: Text(texts[_selectedIndex]),
        gradient: LinearGradient(
          colors: [
            Color(0xFFeea184),
            Color(0xFF24978a),
          ],
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 14,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.pie_chart),
            label: 'summary',
            tooltip: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet),
            label: 'transaction',
            tooltip: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.tune),
            label: 'setting',
            tooltip: "",
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xFF00968a),
        onTap: _onItemTapped,
      ),
      floatingActionButton: _selectedIndex == 1
          ? FloatingActionButton(
              tooltip: 'Add transaction',
              backgroundColor: Color(0xFF24978a),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditTransaction(
                      transactionID: null,
                    ),
                  ),
                );
              },
              child: Icon(Icons.add),
            )
          : null, // This trailing
    );
  }
}
