import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'myApp.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

//flutter run -d chrome --web-renderer html --no-sound-null-safety